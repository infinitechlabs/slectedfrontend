import { Component, OnInit,AfterViewInit } from '@angular/core';
import { DataservicesService } from '../dataservices.service';
import { AuthService } from '../auth.service';
import { CrudService } from '../crud.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { environment } from '../../environments/environment';
import { NumberformatePipe } from '../numberformate.pipe';
import { TimeoutError } from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  
})





export class SidebarComponent implements OnInit,AfterViewInit {
  
  username: any;
  salaryHide="password";
  firstName
  lastName
  rating_avg:any;
  salary: any ='0';
  salaryAmount: any;
  position: string;
  CurrentCompnay: string;
  PreviousCompnay: any;
  fileData: File = null;
  uploadedFiles: Array < File > ;
  PreviousCompnaynew: any;
  Edu_subject: any;
  Edu_degree: any;
  uid: any;
  profilePhoto: Object;
  percentage: any;
  basePath:any;
  websitePath: string;
  errormsg: boolean;
  successmsg: boolean;
  
  constructor(public dt:DataservicesService,public auth:AuthService,public crud:CrudService,private http: HttpClient,private router:Router,private formatPipe:NumberformatePipe) { 
    this.basePath=environment.serverPath;
    this.websitePath=environment.websitePath;


  }

  fileChange(element) {
    this.uploadedFiles = element.target.files;
    
    

}
 
upload() {

  
  let formData = new FormData();
  let getUserData=this.auth.getLoggedInDetails();
  formData.append("user_id",getUserData['uid']);
  
  for (var i = 0; i < this.uploadedFiles.length; i++) {
    if(this.uploadedFiles[i].type=='image/png' || this.uploadedFiles[i].type=='image/gif' || this.uploadedFiles[i].type=='image/jpg' || this.uploadedFiles[i].type=='image/jpeg' )
    {
      formData.append("uploads[]", this.uploadedFiles[i], this.uploadedFiles[i].name);
      this.crud.postImage('/users/upload/', formData)
      .subscribe((response) => {
        console.log(response)
           this.profilePhoto=response;
           
          window.location.reload();
          },(err)=>{
            console.log(err);
          })
          this.successmsg=true;
          setInterval(() => {  
            this.successmsg=false;
            
          },5000);
          //this.errormsg="Profile photo updated";
      //return true;
    }
    else
    {
      // this.errormsg="Invalid file format allow only jpg/jpeg";
      this.errormsg=true;
          setInterval(() => {  
            this.errormsg=false;
            
          },5000);
      return false;
    }

      
  }
  // formData.append("uploads", this.uploadedFiles[], this.uploadedFiles[i].name);
 
 
}


  ngOnInit() {
    //this.firstName="Girish";
    //this.lastName="Phalak"
    
    let getUserData=this.auth.getLoggedInDetails();
    this.uid=getUserData['uid'];
    this.crud.postData('/users/getuserdata',{user_id:this.uid}).subscribe(
      (res:any)=>
      {
        if(res.length > 0)
        {
          this.username=res[0]['user_name'].split(" ",2);
          this.firstName=this.username[0];
          this.lastName=this.username[1];
          this.profilePhoto=res[0]['user_profileimg'];
          this.rating_avg=res[0]['rating_avt']; 
          this.rating_avg=parseFloat(res[0]['rating_avt']).toFixed(3);
          // this.rating_avg=this.formatPipe.transform(parseFloat(res[0]['rating_avt']).toFixed(2))
          if(res[0]['user_salary'] =="0")
          {
            this.salary="0€";
          }
          else
          {
            this.salary=res[0]['user_salary'];
          }
        }
      },
      (err)=>
      { 
        //console.log(err);
      }

    )
    /*user Job data*/
    this.jobData(this.uid)
    /*Education Data */
    this.EducationData(this.uid) 
    this.ProfilePercentage(this.uid);

    this.dt.data_trans_object.subscribe(res=>{
      //console.log(res);
      if(Array.isArray(res)){
        this.username=res['MyName'].split(" ",2);
      this.firstName=this.username[0];
      this.lastName=this.username[1];
      }
      

    },
      err=>{
    }
    )
    this.dt.data_trans_object.subscribe(res=>{
        this.jobData(this.uid)
        this.ProfilePercentage(this.uid);
    },
      err=>{
        //console.log(err);
      }
    ) 
    this.dt.data_trans_object.subscribe((res)=>{
      
    })

    /*GET user Education Data*/
    this.dt.data_trans_object.subscribe((res)=>{
      this.crud.postData('/users/getuserEducation',{user_id:getUserData['uid']}).subscribe(
        (res)=>{
          if(res != "" )
          {
            this.Edu_subject=res[0]['edu_subject'];
            this.Edu_degree= res[0]['edu_graduation'];
          }
          else
          {
            this.Edu_subject="";
            this.Edu_degree= "";
          }
         
          this.ProfilePercentage(this.uid);
        },
        (err)=>{
          
          this.Edu_subject="";
          this.Edu_degree= "";
  
        }
      )
      },
      (err)=>{
        //console.log(err);
      }
    )
  }

  /*Job data*/
  jobData(uid)
  {
     /*Get users Current Company*/
     this.crud.postData('/users/usercurrentCompany',{user_id:uid}).subscribe(
      (res)=>{
        
        this.position=res[0]['job_position'];
       
        this.CurrentCompnay=res[0]['job_company'];
        this.crud.postData('/users/profilePercentage',{user_id:uid}).subscribe(
          (res)=>{
            
            this.percentage=res[0]['tot'];
            
          },
          (err)=>{
            this.percentage="10";
          }
      
        )
      },
      (err)=>{
        
        this.position="";
        this.CurrentCompnay="";

      }
    )
    
    /*Get users Previous Company*/
    this.crud.postData('/users/userprivousCompany',{user_id:uid}).subscribe(
      (res)=>{
        
        this.PreviousCompnay=res;
        this.crud.postData('/users/profilePercentage',{user_id:uid}).subscribe(
          (res)=>{
           
            this.percentage=res[0]['tot'];
            
          },
          (err)=>{
            this.percentage="10";
          }
      
        )
      },
      (err)=>{
        //console.log(err)
        //this.position="NA";
        this.PreviousCompnay="";

      }
    )
  }
  /*Job data end*/
  EducationData(uid)
  {
    this.crud.postData('/users/getuserEducation',{user_id:uid}).subscribe(
      (res:any)=>{
        
        if(res.length > 0)
        {
          
          this.Edu_subject=res[0]['edu_subject'];
          this.Edu_degree= res[0]['edu_graduation'];
        }
        this.crud.postData('/users/profilePercentage',{user_id:uid}).subscribe(
          (res)=>{
            
            this.percentage=res[0]['tot'];
            
          },
          (err)=>{
            this.percentage="10";
          }
      
        )
      },
      (err)=>{
        this.Edu_subject="";
        this.Edu_degree= "";

      }
    )
  }
  ProfilePercentage(uid)
  {
    
        this.crud.postData('/users/profilePercentage',{user_id:uid}).subscribe(
          (res)=>{
            
            this.percentage=res[0]['tot'];
            
          },
          (err)=>{
            this.percentage="10";
          }
      
        )
      
  }
  addSalary(value)
  {
    
    let getUserData=this.auth.getLoggedInDetails();
    this.salaryAmount=value.split(" ",2);
    this.salaryAmount=this.salaryAmount[0];
    console.log(this.salaryAmount);
    this.crud.postData('/users/updateSalary',{user_id:getUserData['uid'],user_salary:this.salaryAmount}).subscribe(
      (res)=>{
        
        // this.ProfilePercentage(getUserData['uid']);
        this.crud.postData('/users/profilePercentage',{user_id:getUserData['uid']}).subscribe(
          (res)=>{
            
            this.percentage=res[0]['tot'];
            
          },
          (err)=>{
            this.percentage="10";
          }
      
        )
      },
      (err)=>
      {
       
        this.crud.postData('/users/profilePercentage',{user_id:getUserData['uid']}).subscribe(
          (res)=>{
            
            this.percentage=res[0]['tot'];
            
          },
          (err)=>{
            this.percentage="10";
          }
      
        )
      }
    )
  }
  logout()
  {
    this.crud.select('/users/logout').subscribe((res)=>{
      if(res=='done')
      {
        this.auth.logout()
        this.dt.loginDetails(false);
        // this.router.navigate(['']);
        window.location.href='/sign-up';
      }

     },(err)=>{

      })
  }
  onSubmit()
  {

  }
  ngAfterViewInit(): void {
    $(document).ready(function(){
      $(".profilebutton").click(function(){
        $(this).hide();
        $('.otherboxwrap').show();
      });
      $(".btn").click(function(){
        $(".profilebutton").show();
        $('.otherboxwrap').hide();
      });
    
      $(".usermenu").click(function(){
        $('.menuopenbox').show();
      });
    
      $(".usermenuclose").click(function(){
        $(".menuopenbox").hide();
        $('.userprofile2').show();
      });

      $(".profileImg").click(function () {
        //alert(222);
        $(".profilepopup").show();
        });
      $(".close").click(function(){
        $(".profilepopup").hide();
        window.location.reload();
      });  

    });
  } 
  myFunction()
  {
    
    //this.salaryHide="text";
    if(this.salaryHide=='text')
    {
      this.salaryHide="password";
    }
    else
    {
      this.salaryHide="text";
    }
  }
  //  myFunction() {
  //   var x = document.getElementById("myInput");
  //   if (x.type === "password") {
  //     x.type = "text";
  //   } else {
  //   x.type = "password";
  //   }
  //   }

}
