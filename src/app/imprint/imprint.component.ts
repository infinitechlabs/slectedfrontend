import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../navbar.service';
import { TranslateService } from '@ngx-translate/core';
import {AuthService} from '../auth.service';
import { CrudService } from '../crud.service';
import { DataservicesService } from '../dataservices.service';
@Component({
  selector: 'app-imprint',
  templateUrl: './imprint.component.html',
  styleUrls: ['./imprint.component.css']
})
export class ImprintComponent implements OnInit {
  showmenu: boolean;

  constructor(public nav: NavbarService,private translate: TranslateService,private auth:AuthService,public crud:CrudService,public dt:DataservicesService) {
    this.nav.BeforeLogin(true);
    translate.setDefaultLang('en');
   }
   switchLanguage(language: string) {
    this.translate.use(language);
  }

  ngOnInit() {
    this.showmenu=false
    this.auth.getLoggedInDetails();
    if(this.auth.getLoggedInDetails())
    {
      this.showmenu=true;
    }
  }
  logout()
  {
    this.crud.select('/users/logout').subscribe((res)=>{
      if(res=='done')
      {
        this.auth.logout()
        this.dt.loginDetails(false);
        // this.router.navigate(['']);
        window.location.href='/';
      }

     },(err)=>{

      })
  }

}
