import { Component, OnInit } from '@angular/core';
import { DataservicesService } from '../dataservices.service';
import { AuthService } from '../auth.service';
import { CrudService } from '../crud.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(public dt:DataservicesService,public auth:AuthService,public crud:CrudService,private http: HttpClient,private router:Router) { }

  ngOnInit() {
    this.crud.select('/users/logout').subscribe((res)=>{
      if(res=='done')
      {
        this.auth.logout()
        this.dt.loginDetails(false);
        // this.router.navigate(['']);
        window.location.href='/';
      }

     },(err)=>{

      })
  }

}
