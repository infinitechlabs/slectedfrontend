import { AfterViewInit, Directive, ElementRef, NgZone } from '@angular/core';

declare var $:any
@Directive({
  selector: '[appDatepicker]',
  exportAs:"datepicker"
})
export class DatepickerDirective implements AfterViewInit {
  mydate: any;

  constructor(private el:ElementRef,private ngZone:NgZone) { }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.ngZone.runOutsideAngular(()=>{
      $(this.el.nativeElement).datepicker({
        onSelect:(date)=>{
          this.setDate(date);
        }
      })
    });
    
    
  }
  setDate(date)
  {
    this.mydate=date
  }
}
