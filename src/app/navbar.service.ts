import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class NavbarService {
  public afterLoginScreen = new Subject();
  public beforeLoginScreen = new Subject();

  constructor() { }
  AferLogin(rec)
  {
    this.afterLoginScreen.next(rec)
  }
  BeforeLogin(rec)
  {
    this.beforeLoginScreen.next(rec)
  }

}
