import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../navbar.service';
import {CrudService} from '../crud.service';
import { DataservicesService } from '../dataservices.service';
import {FormBuilder,FormGroup,FormControl,Validators,NgForm} from '@angular/forms'
import { RouterModule,Router } from '@angular/router';
import {AuthService} from '../auth.service';

import { DeviceDetectorService } from 'ngx-device-detector';
@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {
  signupForm:FormGroup;  
  Emailid:string;
  msg: Object;
  deviceInfo: any;
  constructor(public nav: NavbarService,private fb:FormBuilder,private router:Router,private dt:DataservicesService, private crud:CrudService,private auth:AuthService, private deviceService: DeviceDetectorService) {
    this.nav.BeforeLogin(true)
    this.signupForm=fb.group({
      
      Emailid:['',[Validators.required,Validators.email]]
     
    })
  }
  PostData(value)
  {
    let getUserData=this.auth.getLoggedInDetails();
    //console.log(getUserData);
    this.deviceInfo = this.deviceService.getDeviceInfo();
    //console.log(this.deviceInfo);
    //console.log(this.deviceInfo['os']+" "+ this.deviceInfo['browser']+" "+ this.deviceInfo['browser_version']);
    var formValue={
      'user_email':value['Emailid'],
      'user_id'   :getUserData['uid'],
      'user_name'   :getUserData['fullname'],
      'OS'        :this.deviceInfo['os'],
      'browser'  :this.deviceInfo['browser'],
      'version'   :this.deviceInfo['browser_version']
     }
    
    this.crud.postData('/users/resetpass_email',formValue).subscribe((res) =>{
      this.router.navigate(['forgotpassword-mail']);
     
    },
    (err) =>{
    
     
    }
  );
  }  
  ngOnInit() {
  }

}
