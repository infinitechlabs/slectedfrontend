import { Component, OnInit,AfterViewInit,ViewChild, ElementRef } from '@angular/core';
import { NavbarService } from '../navbar.service';
import { DataservicesService } from '../dataservices.service';
import {NgForm, FormGroup,FormBuilder,Validators} from '@angular/forms';
import { CrudService } from '../crud.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import {formatDate,DatePipe} from '@angular/common';
import * as moment from 'moment'; 

import { DatepickerOptions, NgDatepickerModule } from 'ng2-datepicker';
import * as enLocale from 'date-fns/locale/en';
import * as frLocale from 'date-fns/locale/fr';
declare var $: any;
@Component({
  selector: 'app-skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.css']
})
export class SkillComponent implements OnInit,AfterViewInit {

  options: DatepickerOptions = {
    minYear: 1970,
    maxYear: 2021,
    displayFormat: 'MMM D[,] YYYY',
    barTitleFormat: 'MMMM YYYY',
    dayNamesFormat: 'dd',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    locale: enLocale,
    minDate: new Date(Date.now()), // Minimal selectable date
    maxDate: new Date(Date.now()),  // Maximal selectable date
    barTitleIfEmpty: 'Click to select a date',
    placeholder: 'Click to select a date', // HTML input placeholder attribute (default: '')
    addClass: 'form-control', // Optional, value to pass on to [ngClass] on the input field
    addStyle: {}, // Optional, value to pass to [ngStyle] on the input field
    fieldId: 'job_period_from', // ID to assign to the input field. Defaults to datepicker-<counter>
    useEmptyBarTitle: false, // Defaults to true. If set to false then barTitleIfEmpty will be disregarded and a date will always be shown 
  };


  testform;
  position: any;
  CurrentCompnay: any="";
  PreviousCompnay: string;
  PreviousComp: string;
  skillList: any;
  candidateSkills= new Array();
  jobForm:FormGroup
  msg: boolean;
  msgerror:boolean;
  educationForm:FormGroup
  Education_msg: boolean;
  userskills:any;
  PreviousCompnayId: any;
  CurrentCompnayId: any;
  skillmsg: boolean;
  Education_errormsg: boolean;
  skill_errormsg: boolean;
  year:any[]

  @ViewChild('sd') sdate : ElementRef;
  @ViewChild('ed') edate : ElementRef;
  year_range: any[];
  TodaysDate: string;
  testdate: Date;

  constructor(public nav: NavbarService,public dt:DataservicesService,private fb:FormBuilder,private crud:CrudService,private auth:AuthService,private router:Router,public datepipe: DatePipe
    ) {
      this.TodaysDate =formatDate(new Date(), 'dd/MM/yyyy', 'en');
     this.nav.AferLogin(true)
     this.testform=fb.group({
      username:['',[Validators.required]],
    
     })

     this.jobForm=fb.group({
      job_company:['',[Validators.required]],
      job_position:['',[Validators.required]],
      job_period_from:[''],
      job_period_to:[''],
      job_current:['true']

    })

    this.educationForm=fb.group({
      university:['',[Validators.required]],
      subject:['',[Validators.required]],
      graduation:['',[Validators.required]],
      year:['',[Validators.required]]
     })
    }
  

  ngOnInit() {
   
    this.msg=false;
    this.msgerror=false
    var year = new Date().getFullYear();
    var range = [];
    // range.push(year);
    for (var i =1; i < year; i++) {
        range.push(year - i);
       
    }
   
    const currentYear = (new Date()).getFullYear();
    const range1 = (start, stop, step) => Array.from({ length: (stop - start) / step + 1}, (_, i) => start + (i * step));

      this.year_range=range1(currentYear, currentYear - 60, -1);
    
    let getUserData=this.auth.getLoggedInDetails();
        /*Get users Current Company*/
        this.crud.postData('/users/usercurrentCompany',{user_id:getUserData['uid']}).subscribe(
          (res)=>{
           
            this.position=res[0]['job_position'];
            this.CurrentCompnay=res[0]['job_company'];
            this.CurrentCompnayId=res[0]['job_id'];
          },
          (err)=>{
            
            this.position="NA";
            this.CurrentCompnay="";
            this.CurrentCompnayId="";
    
          }
        )
     
        /*Get users Previous Company*/
    this.crud.postData('/users/userprivousCompany',{user_id:getUserData['uid']}).subscribe(
          (res)=>{
            //this.position=res[0]['job_position'];
            
            this.PreviousCompnay=res[0]['job_company'];
            this.PreviousCompnayId=res[0]['job_id'];
            },
          (err)=>{
            
            
            this.PreviousCompnay="";
            this.PreviousCompnayId="";

          }
        )

    /*Get users Skills */
    // console.log(getUserData['uid']);
    this.crud.postData('/users/getuserskill',{"user_id":getUserData['uid']}).subscribe(
      (res)=>{
        this.userskills=res;

         for(var i=0; i < this.userskills.length; i++)
        {
          
         
            this.candidateSkills.push({user_id:getUserData['uid'],candidateSkill: res[i]['userskill_skillid'] })
        }
       

        },
      (err)=>{
        

      }
    )


        /*Get users Previous Company*/
        this.crud.select('/masters/getSkill').subscribe(
          (res)=>{
            
            this.skillList=res
            
            },
          (err)=>{
          }
        )

        $(
          function() {
              $("#startDate").datepicker( {dateFormat : "mm/dd/yy"});
              $("#endDate").datepicker( {dateFormat : "mm/dd/yy"});
          }
      );   
      
  }
  
  PostData(value)
  {
    
    this.dt.shareData({MyName:value['username']})
  }
  dateChanged(date){
    return  moment(date).format('YYYY-MM-DD')
  }
  PostJobData(value)
  {
    
    let startDate = this.sdate.nativeElement.value;
    let endDate = this.edate.nativeElement.value;
   
    // stop here if form is invalid
    
      const id=value;
      let getUserData=this.auth.getLoggedInDetails();
      const toDate=endDate;
      const fromDate=startDate;
      if(value.job_current=="" || value.job_current=='false')
      {
        value.job_current='false';
        this.PreviousCompnay=value.job_company;
      }
      else
      {
        this.CurrentCompnay=value.job_company;   
      }
      
      var formValue={
              'job_userid':getUserData['uid'],
              'job_company':value.job_company,
              'job_position':value.job_position,
              'job_period_from':this.dateChanged(fromDate),
              'job_period_to':this.dateChanged(endDate),
              'job_current':value.job_current
          }
        
      this.crud.postData('/users/addUserJob',formValue).subscribe((res) =>{
      this.msg=true;
      setInterval(() => {  
        this.msg=false;
      },5000);
      this.PreviousCompnayId=res['insertId'];
      this.dt.shareData({PreviousJobs:value.job_company,job_status:value.job_current,job_position:value.job_position});
      this.jobForm.reset();
      },
      (err) =>{
        this.msgerror=true;
        setInterval(() => {  
          this.msgerror=false;
        },5000);
      }
    );    
  }
 
  /*Remove Compnay*/
  remove_Company(value)
  {
    
    var target = event.target || event.srcElement || event.currentTarget;
    
    //var idAttr = target.attributes.id;
    //var value = idAttr.nodeValue;
   
    let getUserData=this.auth.getLoggedInDetails();
    var formValue={
      'job_userid':getUserData['uid'],
      'job_id':value
    }
    
    this.crud.postData('/users/deleateCompany',formValue).subscribe((res) =>{
      /*Get updated Previous Company List Start*/
        this.crud.postData('/users/userprivousCompany',{user_id:getUserData['uid']}).subscribe(
          (res)=>{
         
            this.PreviousCompnay=res[0]['job_company'];
            this.PreviousCompnayId=res[0]['job_id'];
            this.dt.shareData({PreviousJobs:res[0]['job_company'],job_status:res[0]['job_current'],job_position:res[0]['job_position']});
            
            },
          (err)=>{
            this.PreviousCompnay="";
            this.PreviousCompnayId="";
          }
        )
        this.crud.postData('/users/usercurrentCompany',{user_id:getUserData['uid']}).subscribe(
          (res)=>{
            
            this.position=res[0]['job_position'];
            this.CurrentCompnay=res[0]['job_company'];
            this.CurrentCompnayId=res[0]['job_id'];
            this.dt.shareData({PreviousJobs:res[0]['job_company'],job_status:res[0]['job_current'],job_position:res[0]['job_position']});
          },
          (err)=>{
            
            this.position="NA";
            this.CurrentCompnay="";
            this.CurrentCompnayId=res[0]['job_id'];
    
          }
        )


      },
      (err) =>{
      }
    /*Get Updated Previous Company List End*/  
    )  
    
  }

  /*Post Education Data*/
  PostEducationData(value)
  {
    
    let getUserData=this.auth.getLoggedInDetails();

      var formValue={
        'userid':getUserData['uid'],
        'university':value.university,
        'subject':value.subject,
        'graduation':value.graduation,
        'year':value.year
       
      }
      this.crud.postData('/users/adduserEducation',formValue).subscribe((res) =>{
        // this.Education_msg="Ausbildung der Profildaten";
        this.Education_msg=true;
        setInterval(() => {  
          this.Education_msg=false;
          
        },5000);

        this.dt.shareData({EducationSubject:value.subject,Education_graduation:value.graduation});
        
        this.educationForm.reset();
        this.educationForm.value.year = undefined;
        },
        (err) =>{
        this.Education_errormsg=true;
        setInterval(() => {  
          this.Education_errormsg=false;
          
        },5000);
         
        }
      );   
  }
  searchSkills(value)
  { 
    
    if(value.length > 2)
    {
      this.crud.postData('/masters/getSkillfilter',{skill:value}).subscribe(
        (res)=>{
          //this.position=res[0]['job_position'];
          this.skillList=res
          },
        (err)=>{
          //this.position="NA";
          this.skillList="NA";
  
        }
      )
    }
    else
    {
      /*Get users Previous Company*/
      this.crud.select('/masters/getSkill').subscribe(
        (res)=>{
          this.skillList=res
          //this.position=res[0]['job_position'];
          //this.PreviousCompnay=res[0]['job_company'];
          },
        (err)=>{
          
        }
      )
    }
  }

  /*Skill checkbox check event*/

  selChk(val) 
  {
    const { length } = this.candidateSkills;
  const id = length + 1;
  let getUserData=this.auth.getLoggedInDetails();
  const found = this.candidateSkills.some(el => el.candidateSkill === val);
  if (!found) this.candidateSkills.push({user_id:getUserData['uid'],candidateSkill: val });
    
   }
   SkillAdded()
   {
    //alert(232);
    let getUserData=this.auth.getLoggedInDetails();
     this.crud.postData1('/users/adduserskill',{user_id:getUserData['uid'],candidateSkill:this.candidateSkills}).subscribe(
      (res)=>{

        this.skillmsg=true;
        setInterval(() => {  
          this.skillmsg=false;
          
        },5000);
        this.dt.shareData({user_id:getUserData['uid']});
      },
      (err)=>{
        
        this.skill_errormsg=true;
        setInterval(() => {  
          this.skill_errormsg=false;
          
        },5000);
        this.dt.shareData({user_id:getUserData['uid']});
      }
    )
   }
   RemoveChk(val)
   {
    let arr=this.candidateSkills
    let getUserData=this.auth.getLoggedInDetails();
    this.crud.postData1('/users/deleateusersSkill',{user_id:getUserData['uid'],candidateSkill:val}).subscribe(
      (res)=>{ 
        
      var filtered = arr.filter(function(arr) { 
        return arr.candidateSkill !== val;  
      });
      
      this.candidateSkills=filtered;
    },(err)=>{
      //console.log(err);

     });
    
    }
  ngAfterViewInit(): void {
    
    $(document).ready(function () {
      var today = new Date();
      $('.datepicker').datepicker({
          format: 'dd.mm.yyyy',
          autoclose:true,
          endDate: "today",
          maxDate: "today",
          changeMonth: true,
           changeYear: true,
           yearRange: "-100:+0",
      }).on('changeDate', function (ev) {
              $(this).datepicker('hide');
          });


      $('.datepicker').keyup(function () {
          if (this.value.match(/[^0-9]/g)) {
              this.value = this.value.replace(/[^0-9^-]/g, '');
          }
      });
  });
   
    // clean events log
    $('#third_div small').click(function() {
      $('#third_div ul').empty();
    });
    
 



    $('#tabs li a:not(:first)').addClass('inactive');

    $('.container').hide();

    $('.container:first').show();

        

    $('#tabs li a').click(function(){

        var t = $(this).attr('id');

      if($(this).hasClass('inactive')){ //this is the start of our condition 

        $('#tabs li a').addClass('inactive');           

        $(this).removeClass('inactive');

        

        $('.container').hide();

        $('#'+ t + 'C').fadeIn('slow');

     }

    });



    /////////////////////////////////////////////////////////////////////////////



    $('#tabs2 li a:not(:first)').addClass('inactive');

    $('.container2').hide();

    $('.container2:first').show();

        

    $('#tabs2 li a').click(function(){

        var t = $(this).attr('id');

      if($(this).hasClass('inactive')){ //this is the start of our condition 

        $('#tabs2 li a').addClass('inactive');           

        $(this).removeClass('inactive');

        

        $('.container2').hide();

        $('#'+ t + 'C').fadeIn('slow');

     }

    });


  }
  

}

