import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CompareComponent } from './compare/compare.component';
import { PersonaldataComponent } from './personaldata/personaldata.component';
import { SafetyComponent } from './safety/safety.component';
import { SkillComponent } from './skill/skill.component';
import { CheckmailComponent } from './checkmail/checkmail.component';
import { NewaccountComponent } from './newaccount/newaccount.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ForgotpasswordMailComponent } from './forgotpassword-mail/forgotpassword-mail.component';
import { PasswordComponent } from './password/password.component';
import { RatingComponent } from './rating/rating.component';
import { LogoutComponent } from './logout/logout.component';
import { ComparedataComponent } from './comparedata/comparedata.component';
import { EmailverificationComponent } from './emailverification/emailverification.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { YourvalueComponent } from './yourvalue/yourvalue.component';
import { HelpComponent } from './help/help.component';
import { ImprintComponent } from './imprint/imprint.component';
import { DataprotectionComponent } from './dataprotection/dataprotection.component';
import { SocialLoginComponent } from './social-login/social-login.component';
import { LoginComponent } from './admin/login/login.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  {path: '', redirectTo: 'sign-up', pathMatch: 'full'},
  //{path: '**', component: PagenotfoundComponent},
  {path:'',component:CheckmailComponent},
  {path:'sign-up',component:CheckmailComponent},
  {path:'dashboard',component:DashboardComponent,canActivate:[AuthGuard]},
  {path:'compare',component:CompareComponent,canActivate:[AuthGuard]},
  {path:'personaldata',component:PersonaldataComponent,canActivate:[AuthGuard]},
  {path:'safety',component:SafetyComponent,canActivate:[AuthGuard]},
  {path:'skill',component:SkillComponent,canActivate:[AuthGuard]},
  {path:'sign-in',component:NewaccountComponent},
  {path:'forgotpassword',component:ForgotpasswordComponent},
  {path:'forgotpassword-mail',component:ForgotpasswordMailComponent},
  {path:'password',component:PasswordComponent,canActivate:[AuthGuard]},
  {path:'rating',component:RatingComponent,canActivate:[AuthGuard]},
  {path:'logout',component:LogoutComponent},
  {path:'campairedata/:xyz',component:ComparedataComponent,canActivate:[AuthGuard]},
  {path:'yourvalue',component:YourvalueComponent,canActivate:[AuthGuard]},
  {path:'help',component:HelpComponent},
  {path:'imprint',component:ImprintComponent},
  {path:'emailverification/:xyz',component:EmailverificationComponent},
  {path:'resetpassword/:xyz',component:ResetpasswordComponent},
  {path:'dataprotect',component:DataprotectionComponent},
  {path:'linkedInLogin',component:SocialLoginComponent},
  {path:'admin/login',component:LoginComponent},
  {path:'admin/dashboard',component:AdminDashboardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
