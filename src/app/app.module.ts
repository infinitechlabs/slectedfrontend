import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule,HttpClient } from '@angular/common/http';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader'


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LoginheaderComponent } from './loginheader/loginheader.component';
import { LoginfooterComponent } from './loginfooter/loginfooter.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CompareComponent } from './compare/compare.component';
import { PersonaldataComponent } from './personaldata/personaldata.component';
import { SafetyComponent} from './safety/safety.component';
import { SkillComponent } from './skill/skill.component';
import { BeforeloginFooterComponent } from './beforelogin-footer/beforelogin-footer.component';
import { RightsideFooterComponent } from './rightside-footer/rightside-footer.component';
import { CheckmailComponent } from './checkmail/checkmail.component';
import { NewaccountComponent } from './newaccount/newaccount.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ForgotpasswordMailComponent } from './forgotpassword-mail/forgotpassword-mail.component';
import { PasswordComponent } from './password/password.component';
import { RatingComponent } from './rating/rating.component'
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { Ng5SliderModule } from 'ng5-slider';
import { LogoutComponent } from './logout/logout.component';
import { ComparedataComponent } from './comparedata/comparedata.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { EmailverificationComponent } from './emailverification/emailverification.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { DataprotectionComponent } from './dataprotection/dataprotection.component';
import { ImprintComponent } from './imprint/imprint.component';
import { HelpComponent } from './help/help.component';
import { YourvalueComponent } from './yourvalue/yourvalue.component';
import { NumberformatePipe } from './numberformate.pipe';
import { SocialLoginComponent } from './social-login/social-login.component';
import { PasswordStrengthBarComponent } from './password-strength-bar/password-strength-bar.component';
import { NgToggleModule } from 'ng-toggle-button/';
import { LoginComponent } from './admin/login/login.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { DatepickerDirective } from './diretives/datepicker.directive';
import {DatePipe} from '@angular/common';
import { NgDatepickerModule } from 'ng2-datepicker';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { AuthGuard } from './auth.guard';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    LoginheaderComponent,
    LoginfooterComponent,
    DashboardComponent,
    CompareComponent,
    PersonaldataComponent,
    SafetyComponent,
    SkillComponent,
    BeforeloginFooterComponent,
    RightsideFooterComponent,
    CheckmailComponent,
    NewaccountComponent,
    ForgotpasswordComponent,
    ForgotpasswordMailComponent,
    PasswordComponent,
    RatingComponent,
    LogoutComponent,
    ComparedataComponent,
    EmailverificationComponent,
    ResetpasswordComponent,
    DataprotectionComponent,
    ImprintComponent,
    HelpComponent,
    YourvalueComponent,
    NumberformatePipe,
    SocialLoginComponent,
    PasswordStrengthBarComponent,
    LoginComponent,
    AdminDashboardComponent,
    DatepickerDirective,
    PagenotfoundComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AutocompleteLibModule,
    Ng5SliderModule,
    NgToggleModule,
    NgDatepickerModule,
    NgCircleProgressModule.forRoot({
      radius: 60,
  space: -10,
  outerStrokeGradient: true,
  outerStrokeWidth: 10,
  outerStrokeColor: "#4882c2",
  outerStrokeGradientStopColor: "#53a9ff",
  innerStrokeColor: "#e7e8ea",
  innerStrokeWidth: 10,
  animateTitle: false,
  animationDuration: 1000,
  showBackground: false,
  clockwise: false,
  startFromZero: false
    }),
    DeviceDetectorModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [NumberformatePipe,DatePipe,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
