import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-loginfooter',
  templateUrl: './loginfooter.component.html',
  styleUrls: ['./loginfooter.component.css']
})
export class LoginfooterComponent implements OnInit {

  constructor(private translate: TranslateService) { 
    translate.setDefaultLang('en');

  }
  switchLanguage(language: string) {
    this.translate.use(language);
  }

  ngOnInit() {
  }

}
