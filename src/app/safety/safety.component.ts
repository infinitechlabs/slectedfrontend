import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../navbar.service';
import { DataservicesService } from '../dataservices.service';
import {NgForm, FormGroup,FormBuilder,Validators} from '@angular/forms';
import { CrudService } from '../crud.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-safety',
  templateUrl: './safety.component.html',
  styleUrls: ['./safety.component.css']
})
export class SafetyComponent implements OnInit {
  saftyForm
  msg:string;
  oldPass:string
  newPass:string
  confirmPass:string
  errmsg: boolean;
  successmsg: boolean;
  oldpassword_mismatch: boolean;
  public barLabel: string = "";
  public account = {  
    
    password: null  
  };  
  constructor(public nav: NavbarService,public dt:DataservicesService,private fb:FormBuilder,private crud:CrudService,private auth:AuthService,private router:Router) 
  {
    this.nav.AferLogin(true)
    this.saftyForm=fb.group({
      oldPass:['',Validators.required],
      newPass:['',Validators.required],
      confirmPass:['',Validators.required]
    })
  }

  ngOnInit() {
  }
  checkCurrent_password(formValue)
  {
    //console.log(formValue);
    this.crud.postData('/users/checkPassword',formValue).subscribe(res=>{
      //console.log(res);
        this.crud.postData('/users/updatePassword',formValue).subscribe((result) =>{
          
          //this.msg="Password update";
          this.successmsg=true;
          setInterval(() => {  
            this.successmsg=false;
            
          },5000);
        
        },
        (err) =>
        {
          //console.log(err);
        
        }
      );
      
    },(err)=>{
      //console.log(err);
      this.oldpassword_mismatch=true;
      setInterval(() => {  
        this.oldpassword_mismatch=false;
        
      },5000);
    })
  }
  SubmitData(value)
  {
      let getUserData=this.auth.getLoggedInDetails();
      if(value.newPass == value.confirmPass){
        var formValue={
          'user_id':getUserData['uid'],
          'user_oldpass':value.oldPass,
          'user_pass':value.newPass
          // 'user_dob':value.confirmPass
          
      }
      this.checkCurrent_password(formValue);
      }
      else{
        this.errmsg=true;
        setInterval(() => {  
          this.errmsg=false;
          
        },5000);
      }
    
  }

}
