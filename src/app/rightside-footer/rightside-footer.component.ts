import { Component, OnInit,AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-rightside-footer',
  templateUrl: './rightside-footer.component.html',
  styleUrls: ['./rightside-footer.component.css']
})
export class RightsideFooterComponent implements OnInit,AfterViewInit {

  constructor() { }

  ngOnInit() {
  }
  ngAfterViewInit(): void {
    $(".xing").click(function () 
    {
      $(".xingpopup").show();
    });
    $(".linkedin").click(function () 
    {
    
      $(".linkedinpopup").show();
    });
    
    $(".close").click(function () {
      $(".xingpopup,.linkedinpopup").hide();
    });
  }  
}
