import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightsideFooterComponent } from './rightside-footer.component';

describe('RightsideFooterComponent', () => {
  let component: RightsideFooterComponent;
  let fixture: ComponentFixture<RightsideFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightsideFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightsideFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
