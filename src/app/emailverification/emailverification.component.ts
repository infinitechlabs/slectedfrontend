import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../navbar.service';
import {CrudService} from '../crud.service';
import {AuthService} from '../auth.service';
import { ActivatedRoute } from '@angular/router';
import { DataservicesService } from '../dataservices.service';
@Component({
  selector: 'app-emailverification',
  templateUrl: './emailverification.component.html',
  styleUrls: ['./emailverification.component.css']
})
export class EmailverificationComponent implements OnInit {

  constructor(public nav: NavbarService, private crud:CrudService,private auth:AuthService,private router:ActivatedRoute, private dt:DataservicesService) 
  {
    this.nav.BeforeLogin(true);
  }

  ngOnInit() 
  {
    var id= this.router.snapshot.params;
    this.crud.postData('/users/verificationAction',{userid:id['xyz']}).subscribe(
      (res)=>{
        //this.dt.shareData({})
        //console.log('Email verified');
      },
      (err)=>{ 

      }
      )
  }
}