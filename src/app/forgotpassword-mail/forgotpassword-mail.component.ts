import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../navbar.service';
@Component({
  selector: 'app-forgotpassword-mail',
  templateUrl: './forgotpassword-mail.component.html',
  styleUrls: ['./forgotpassword-mail.component.css']
})
export class ForgotpasswordMailComponent implements OnInit {

  constructor(public nav: NavbarService) {this.nav.BeforeLogin(true) }

  ngOnInit() {
  }

}
