import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotpasswordMailComponent } from './forgotpassword-mail.component';

describe('ForgotpasswordMailComponent', () => {
  let component: ForgotpasswordMailComponent;
  let fixture: ComponentFixture<ForgotpasswordMailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgotpasswordMailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotpasswordMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
