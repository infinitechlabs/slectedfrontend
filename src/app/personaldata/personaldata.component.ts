import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NavbarService } from '../navbar.service';

import { DataservicesService } from '../dataservices.service';
import {NgForm, FormGroup,FormBuilder,Validators} from '@angular/forms';
import { CrudService } from '../crud.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import {formatDate} from '@angular/common';
declare var $: any;
import * as moment from 'moment'; 
@Component({
  selector: 'app-personaldata',
  templateUrl: './personaldata.component.html',
  styleUrls: ['./personaldata.component.css']
})
export class PersonaldataComponent implements OnInit {
  Personalform;
  msg: boolean;
  msgerror:boolean;
  personaldata: any;
  user_name: any;
  public user_email1: any;
  status: any;
  user_dob1: any;
  user_name1: any;
  email_msg: boolean;
  email_msg_err: boolean;
  updateEmail: any;
  today:any;
  email_msgsending: boolean;
  constructor(public nav: NavbarService,public dt:DataservicesService,private fb:FormBuilder,private crud:CrudService,private auth:AuthService,private router:Router) { 
    this.nav.AferLogin(true)
    this.Personalform=fb.group({
      user_name:['',[Validators.required]],
      user_email:['',[Validators.required,Validators.email]],
      user_dob:[''],
     })
  }
  @ViewChild('user_dob') sdate : ElementRef;
  ngOnInit() {
    this.user_dob1=formatDate(new Date(), 'dd/MM/yyyy', 'en');
    //this.onValueChanges();
    let getUserData=this.auth.getLoggedInDetails();
    this.crud.postData("/users/getuserdata",{user_id:getUserData['uid']}).subscribe((res)=>{
      this.personaldata=res
      
      this.user_name1=this.personaldata[0].user_name;
      this.user_email1=this.personaldata[0].user_email;
      
      this.user_dob1=formatDate(this.personaldata[0].user_bob, 'dd/MM/yyyy', 'en');
      this.status=this.personaldata[0].user_verify;
      if(this.status=='true')
      {
        this.status=true;
      }
      else
      {
        this.status=false;
      }
      
      this
      //console.log(res);
    },
    (error)=>{
      this.personaldata=error;
    }
      
    )

    $(
      function() {
          $("#user_dob").datepicker( {dateFormat : "mm/dd/yy"});
         
      }
  );   
  }
  
  onValueChanges()
  {
    
    this.Personalform.valueChanges.subscribe(val=>{
     
      let getUserData=this.auth.getLoggedInDetails();
      
      if(val['user_email'] !==getUserData['email'] || val['user_email'] !=='undefined')
      {
        this.updateEmail=val['user_email'];
        
        //this.status=false;
      }
    })
  }

  dateChanged(date){
    return  moment(date).format('YYYY-MM-DD')
  }
  PostData(value)
  {
    
    let getUserData=this.auth.getLoggedInDetails();
    let DobDate = this.sdate.nativeElement.value;
    var formValue={
      'user_id':getUserData['uid'],
      'user_name':value.user_name,
      'user_email':value.user_email,
      'user_dob':this.dateChanged(DobDate), 
      }
    
    this.crud.postData('/users/updatePersonalData',formValue).subscribe((res) =>{
      
      if(getUserData['email'] !==value.user_email)
      {
        this.auth.storeToken(res)
        this.resend_verification();
      }
      this.dt.shareData({user_id:getUserData['uid']});
      this.msg=true;
      setInterval(() => {  
        this.msg=false;
      },5000);
      // this.msg="Aktualisierung der Profildaten";
     
    },
    (err) =>{
   this.msgerror=true;
      setInterval(() => {  
        this.msgerror=false;
      },5000);
     
    }
  );
    this.dt.shareData({MyName:value['user_name']})
  }
  resend_verification()
  {
    this.email_msgsending=true;
    setInterval(() => {  
      this.email_msgsending=false;
    },5000);
    let getUserData=this.auth.getLoggedInDetails();
    
    
    var formValue={
      'user_id':getUserData['uid'],
      'user_name':getUserData['fullname'],
      'user_email':getUserData['email']
    }
    
    this.crud.postData('/users/verification_email',formValue).subscribe((res) =>{
      
      this.email_msg=true;
      setInterval(() => {  
        this.email_msg=false;
      },5000);
    },
    (err) =>{
    
      this.email_msg_err=true;
      setInterval(() => {  
        this.email_msg_err=false;
      },5000);
    }
  );
    // this.dt.shareData({MyName:value['user_name']})

  }
 
  ngAfterViewInit(): void {
    $(
      function() {
          $("#user_dob").datepicker( {dateFormat : "mm-dd-yy"});
          
      }
  );  
  $(document).ready(function () {
    var today = new Date();
    $('.datepicker').datepicker({
        format: 'dd.mm.yyyy',
        autoclose:true,
        endDate: "today",
        maxDate: "today",
        changeMonth: true,
         changeYear: true,
         yearRange: "-100:+0",
    }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });


    $('.datepicker').keyup(function () {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9^-]/g, '');
        }
    });
});
  }    
}
