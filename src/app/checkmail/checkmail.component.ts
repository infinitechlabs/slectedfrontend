import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../navbar.service';
import { DataservicesService } from '../dataservices.service';
import {FormBuilder,FormGroup,FormControl,Validators,NgForm} from '@angular/forms'
import { RouterModule,Router } from '@angular/router';
import {CrudService} from '../crud.service';
import {AuthService} from '../auth.service';

import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-checkmail',
  templateUrl: './checkmail.component.html',
  styleUrls: ['./checkmail.component.css']
})
export class CheckmailComponent implements OnInit {
  show:boolean
  hide: void;
  signupForm:FormGroup;  
  Emailid:string;
  msg: Object;
  emailId: any;
  status: any;
  constructor(public nav: NavbarService,private fb:FormBuilder,private router:Router,private dt:DataservicesService, private crud:CrudService,private auth:AuthService,private translate: TranslateService)
   {
    this.nav.BeforeLogin(true);
    this.signupForm=fb.group({
      Emailid:['',[Validators.required,Validators.email]]
     })

     translate.setDefaultLang('en');
   }
   switchLanguage(language: string) {
    this.translate.use(language);
  }
  ngOnInit() {
    //this.hide=this.nav.hide;
  // this.hide=this.nav.hide();
  }

  PostData(value)
  {
    const id=value.Emailid;
    this.crud.postData('/users/checkEmail',{user_email:id}).subscribe((res)=>{
      if(this.auth.storeToken(res) === true ){
        
        let res =this.auth.getLoggedInDetails();
        this.emailId=res['email'];  
        this.status=res['status_id'];
        if(this.status==0)
        {
         // window.location='/sign-in';  
         this.router.navigate(['/sign-in']);
        }
        else
        {
         // window.location='/password';
          this.router.navigate(['/password']);
          //this.router.navigateByUrl('/password')
        }
        //window.location='/password';
        //this.router.navigate(['/password'])

      }
      // this.auth.checkTokenStatus()
      // if(this.auth.checkTokenStatus()=== false)
      // {
      //   this.router.navigate(['/sign-in'])
      // }
      
    });
   }

}