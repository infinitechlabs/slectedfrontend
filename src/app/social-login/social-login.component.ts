import { Component, OnInit } from '@angular/core';
import { RouterModule,Router, ActivatedRoute } from '@angular/router';
import { CrudService } from '../crud.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-social-login',
  templateUrl: './social-login.component.html',
  styleUrls: ['./social-login.component.css']
})
export class SocialLoginComponent implements OnInit {

  constructor(private router:Router,private activedrouter:ActivatedRoute, private crud:CrudService,private auth:AuthService) { }

  ngOnInit() {
    this.activedrouter.queryParams.subscribe((param)=>{
      //console.log(param.code)
      this.crud.socialLink(param.code).subscribe((res)=>
      {
        //console.log(res);
        this.auth.storeToken(res['access_token']);
        // this.msg = res;

        this.crud.socialUrl('https://api.linkedin.com/v2/me',res['access_token']).subscribe((res)=>{
          //console.log(res)
        },
        (err)=>
        {
          //console.log(err);
        })
        
      })
    })
  }

}
