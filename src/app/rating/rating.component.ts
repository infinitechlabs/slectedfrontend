import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NavbarService } from '../navbar.service';
import { RouterModule,Router } from '@angular/router';



import {FormBuilder,FormGroup,FormControl,Validators,NgForm} from '@angular/forms'
import {AuthService} from '../auth.service';
import { CrudService } from '../crud.service';
import { NumberformatePipe } from '../numberformate.pipe';
@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {
  ratingForm:FormGroup
  rating: any;
  locationValue:number=0.00
  experienceValue:number=0.00
  qualificationValue:number=0.00
  professionValue:number=0.00
  
  keyword = 'name';
  public location = [
    {
      id: 1,
      name: 'Brandenburg',
      value:0.722
    },
    {
      id: 2,
      name: 'Berlin',
      value:0.94
    },
    {
      id: 3,
      name: 'Bayern',
      value:1.6
    },
    {
      id: 4,
      name: 'Baden-Württemberg',
      value:1.1
    },
    {
      id: 5,
      name: 'Bremen',
      value:0.961
    },
    {
      id: 6,
      name: 'Hessen',
      value:1.128
    },
    {
      id: 7,
      name: 'Hamburg',
      value:1.61
    },
    {
      id: 8,
      name: 'Mecklenburg-Vorpommern',
      value:73.5
    },
    {
      id: 9,
      name: 'München',
      value:1.26
    },
    {
      id: 10,
      name: 'Niedersachen',
      value:0.912
    },
    {
      id: 11,
      name: 'Nordrhein-Westfalen',
      value:1.12
    },
    {
      id: 12,
      name: 'Rheinland-Pflaz',
      value:0.983
    },
    {
      id: 13,
      name: 'Schleswig-Holstein',
      value:0.878
    },
    {
      id: 14,
      name: 'Saarland',
      value:0.951
    },
    {
      id: 15,
      name: 'Sachsen',
      value:0.769
    },
    {
      id: 16,
      name: 'Sachen-Anhalt',
      value:0.754
    },
    {
      id: 17,
      name: 'Thüringen',
      value:0.782
    }
  ];
  public experience=[
    {
      id:1,
      name:"0",
      value:1.0
    },
    {
      id:2,
      name:"2",
      value:1.10
    },
    {
      id:3,
      name:"5",
      value:1.15
    },
    {
      id:4,
      name:"7",
      value:1.20
    },
    {
      id:5,
      name:"10",
      value:1.40
    }
  ];
  public qualification=[
    {
      id:1,
      name:'Bachelor',
      value:0.969
    },
    {
      id:2,
      name:'Master',
      value:1.03
    },
    {
      id:3,
      name:'Ausbildung',
      value:0.635
    }
  ]
  public profession=[
    {
      id:1,
      name:'Banken',
      value:52619
    },
    {
      id:2,
      name:'Fahrzeugbau',
      value:49697
    },
    {
      id:3,
      name:'Unternehmensberatung',
      value:50081
    },
    {
      id:4,
      name:'Wirtschaftsprüfung',
      value:50081
    },
    {
      id:5,
      name:'Recht',
      value:50081
    },
    {
      id:6,
      name:'Pharmaindustrie',
      value:48245
    },
    {
      id:7,
      name:'IT',
      value:45561
    },
    {
      id:8,
      name:'Konsumgüter',
      value:46056
    },
    {
      id:9,
      name:'Elektrotechnik',
      value:43313
    },
    {
      id:10,
      name:'Feinmechanik',
      value:43313
    },
    {
      id:11,
      name:'Optik',
      value:43313
    },
    {
      id:12,
      name:'Groß- und Einzelhandel',
      value:42845
    },
    {
      id:13,
      name:'Transport und Logistik',
      value:42090
    },
    {
      id:14,
      name:'Nahrungs- und Genussmittel',
      value:41594
    }
  ];
  ProfshowError: boolean;
  QualishowError: boolean;
  expshowError: boolean;
  locshowError: boolean;
  professionName: any;
  qualificationName: any;
  experienceName: any;
  locationName: any;
  constructor(public nav: NavbarService,private translate: TranslateService,private auth:AuthService,private fb:FormBuilder,private crud:CrudService,private router:Router,private formatPipe:NumberformatePipe) {
    this.nav.BeforeLogin(true)
    translate.setDefaultLang('en');
    this.ratingForm=fb.group({
      profession:['',[Validators.required]],
      qualification:['',[Validators.required]],
      experience:['',[Validators.required]],
      location:['',[Validators.required]],

    })
    
   }
   switchLanguage(language: string) {
    this.translate.use(language);
  }
  ngOnInit() {
    
    let res=this.auth.getLoggedInDetails()
    this.rating='00.00'
    
    //const number = 123456.000;
    //console.log(new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(number));
  }
  selectedprofession(item)
  {
     
    this.ProfshowError=false;
    this.professionValue=item.value;
    this.professionName=item.name;
    this.rating=this.formatPipe.transform(parseFloat(item.value).toFixed(2))
    this.rating=parseFloat(this.rating).toFixed(3);
    //this.getRating();
  }
  selectedQualification(item)
  {
    
    this.QualishowError=false
    this.qualificationValue=item.value;
    this.qualificationName=item.name;
    this.rating=this.qualificationValue*this.professionValue
    this.rating=this.formatPipe.transform(parseFloat(this.rating).toFixed(2))
    this.rating=parseFloat(this.rating).toFixed(3);
    
    //this.getRating();
  }
  selectedExp(item) {
    
    this.expshowError=false
    this.experienceValue=item.value;
    this.experienceName=item.name;
    this.rating=this.experienceValue*this.qualificationValue*this.professionValue
    this.rating=this.formatPipe.transform(parseFloat(this.rating).toFixed(2))
    this.rating=parseFloat(this.rating).toFixed(3);
    
    //this.getRating();
  }
  selectedLocation(item) {
    
    // do something with selected item
    
    this.locshowError=false
    this.locationValue=item.value;
    this.locationName=item.name;
    this.getRating();
  }
  getRating()
  {

    this.rating=this.locationValue*this.experienceValue*this.qualificationValue*this.professionValue
    this.rating=this.formatPipe.transform(parseFloat(this.rating).toFixed(2))
    
    //this.rating=52.057
    //this.rating= parseFloat(this.rating).toFixed(2);
    this.rating=parseFloat(this.rating).toFixed(3);
  }
  PostData(val)
  {
    let getEmail=this.auth.getLoggedInDetails();
    


     if(!this.professionValue)
     {
       this.ProfshowError=true;
     }
     else if(!this.qualificationValue)
     {
       this.QualishowError=true;
     }
     else if(!this.experienceValue)
     {
       this.expshowError=true;
     }
     else if(!this.locationValue)
     {
       this.locshowError=true;
     }
     else
     {
       this.ProfshowError=false;
       this.QualishowError=false;
       this.expshowError=false;
       this.locshowError=false;
       var formValue={
        'rating_userid':getEmail['uid'],
        'rating_pro':this.professionName,
        'rating_qual':this.qualificationName,
        'rating_area':this.locationName,
        'rating_exp':this.experienceName,
        'rating_avt':this.rating
    }
    
    this.crud.postData('/users/adduserRating',formValue).subscribe((res) =>{
        //console.log(res);
       
      },
      (err) =>{
       
       
      }
    );    
    this.router.navigate(['/dashboard'])
     }
     
     
  }


  selectEvent(item) {
    // do something with selected item
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    // do something
  }
  
 

}
