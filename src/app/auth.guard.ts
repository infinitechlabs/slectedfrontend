import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { AuthService } from './auth.service';



@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private _authservice:AuthService, private _router:Router)
    {

    }
    canActivate():boolean{
      if(this._authservice.islogedin())
      {
        return true;
      }
      else
      {
        //return false;
        this._router.navigate(['/sign-up']);
        return false;
      }
    }
}
