import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  getLoggedInDetails()
  {
    if(localStorage.getItem("token"))
    {
      var token=localStorage.getItem("token");
      var decode=jwt_decode(token)
      return decode['data'];
    }
    else
    {
      return false;
    }
  }
  islogedin()
  {
    if(localStorage.getItem("token")==='undefined' || localStorage.getItem("token")==null || localStorage.getItem("token")==='')
    {
      return false;
    }
    else
    {
      return true;
    }
  }
  storeToken(tokendata)
  {
    //console.log(tokendata);
    localStorage.setItem("token",tokendata);
    return true;
  }
  logout()
  {
    localStorage.removeItem("token");
    return true;
  }
  getToken()
  {
    var token=localStorage.getItem("token");
    return token;
  }
  checkTokenStatus()
  {
    const tokendata=localStorage.getItem("token");
    const t= JSON.parse(tokendata);
    
    if(localStorage.getItem("token")==='undefined' || localStorage.getItem("token")==null || localStorage.getItem("token")==='' || t['status']=='0' )
    {
      return false;
    }
    else
    {
      return true;
    }
  }
}
