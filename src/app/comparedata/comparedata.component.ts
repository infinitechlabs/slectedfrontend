import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../navbar.service';
import { environment } from '../../environments/environment';
import { DataservicesService } from '../dataservices.service';
import { AuthService } from '../auth.service';
import { CrudService } from '../crud.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-comparedata',
  templateUrl: './comparedata.component.html',
  styleUrls: ['./comparedata.component.css']
})
export class ComparedataComponent implements OnInit {
  rating_avg: any;
  CandidateSkillsDashboard: any;
  profession: any;
  experience: any;
  imgPath: any;
  comparision_avg: any;
  comparision_profession: any;
  comparision_experience: any;
  comparision_name: any;
  comparision_exp: any;
  comparision_skill: any;
  comparisionimgPath: any;
  basePath:any;
  websitePath: string;
  officeLIST: Array<any> = [
    { "office_id": "1", "officename": "HTML" },
    { "office_id": "2", "officename": "CSS" },
    { "office_id": "3", "officename": "PHP" }
  ];


  office: Array<any> = [
    {
      "office_id": "1",
      "officename": "HTML"
    },
    {
      "office_id": "3",
      "officename": "CSS"
    }
  ];
  newArray: Array<any> = [];
  constructor(public nav: NavbarService,public dt:DataservicesService,public auth:AuthService,public crud:CrudService, private router:ActivatedRoute) { this.nav.AferLogin(true);
    this.basePath=environment.serverPath;
    this.websitePath=environment.websitePath

   
  }


  ngOnInit() {
    let getUserData=this.auth.getLoggedInDetails();
    this.crud.postData('/users/imgPath',{user_id:getUserData['uid']}).subscribe(
      (res)=>
      {
        this.imgPath=res[0]['user_profileimg'];
     
      },
      (err)=>
      {
       // console.log(err);
      }
      )
    
    
    this.crud.postData('/users/getuserdata',{user_id:getUserData['uid']}).subscribe(
      (res)=>
      {
        
        this.rating_avg=res[0]['rating_avt'];
        this.profession=res[0]['rating_pro'];
        this.experience=res[0]['rating_exp'];
      },
      (err)=>
      { 
        //console.log(err);
      }

    )

    /*Compaire Candidate Data*/
    var id= this.router.snapshot.params;
    
    this.crud.postData('/users/getuserdata',{user_id:id['xyz']}).subscribe(
      (res)=>
      {

        //.log(res);
        this.comparisionimgPath=res[0]['user_profileimg'];
        this.comparision_name=res[0]['user_name'];
        this.comparision_avg=res[0]['rating_avt'];
        this.comparision_profession=res[0]['rating_pro'];
        this.comparision_experience=res[0]['rating_exp'];
      },
      (err)=>
      { 
        //console.log(err);
      }

    )
    /*Comapre Candidate Skill*/
    this.crud.postData('/users/getuserskill',{user_id:id['xyz']}).subscribe(
      (res)=>
      {
        this.comparision_skill=res;
        
       
       
      },
      (err)=>
      { 
        
      }

    )

    this.crud.postData('/users/getuserskill',{user_id:getUserData['uid']}).subscribe(
      (res)=>
      {
        
        this.CandidateSkillsDashboard=res;
        for (var i = 0; i < this.CandidateSkillsDashboard.length; i++) {

          var ismatch = false; // we haven't found it yet
    
          for (var j = 0; j < this.comparision_skill.length; j++) {
            
            if (this.CandidateSkillsDashboard[i].userskill_skillid == this.comparision_skill[j].userskill_skillid) {
              // we have found this.officeLIST[i]] in this.office, so we can stop searching
              ismatch = true;
              this.CandidateSkillsDashboard[i].checked = true;//  checkbox status true
              this.newArray.push(this.CandidateSkillsDashboard[i]);
              break;
            }//End if
            // if we never find this.officeLIST[i].office_id in this.office, the for loop will simply end,
            // and ismatch will remain false
          }
          // add this.officeLIST[i] to newArray only if we didn't find a match.
          if (!ismatch) {
            this.CandidateSkillsDashboard[i].checked = false;//  checkbox status false
            this.newArray.push(this.CandidateSkillsDashboard[i]);
          } //End if
        }
        //console.log(this.newArray);
        
        
       
       
      },
      (err)=>
      { 
        //console.log(err);
      }

    )
   
  }
 
 
}

