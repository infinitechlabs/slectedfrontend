import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforeloginFooterComponent } from './beforelogin-footer.component';

describe('BeforeloginFooterComponent', () => {
  let component: BeforeloginFooterComponent;
  let fixture: ComponentFixture<BeforeloginFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeforeloginFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforeloginFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
