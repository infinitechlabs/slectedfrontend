import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-beforelogin-footer',
  templateUrl: './beforelogin-footer.component.html',
  styleUrls: ['./beforelogin-footer.component.css']
})
export class BeforeloginFooterComponent implements OnInit {

  constructor(private translate: TranslateService) { 
    translate.setDefaultLang('en');

  }
  switchLanguage(language: string) {
    this.translate.use(language);
  }
  ngOnInit() {
  }

}
