import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { NavbarService } from '../navbar.service';
import { DataservicesService } from '../dataservices.service';
import { AuthService } from '../auth.service';
import { CrudService } from '../crud.service';
import { NumberformatePipe } from '../numberformate.pipe';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  rating_avg: any;
  CandidateSkillsDashboard: Object;
  profession: any;
  experience: any;
  imgPath: any;
  basePath:any;
  constructor(public router:Router, public nav: NavbarService,public dt:DataservicesService,public auth:AuthService,public crud:CrudService,private formatPipe:NumberformatePipe) { this.nav.AferLogin(true);
    this.basePath=environment.serverPath;
    
  }

  ngOnInit() {
    let getUserData=this.auth.getLoggedInDetails();
    this.auth.getLoggedInDetails();
    console.log(getUserData);
    if(getUserData==false)
    {
      this.router.navigate(['sign-up']);
    }
    else
    {
   
    this.crud.postData('/users/imgPath',{user_id:getUserData['uid']}).subscribe(
      (res)=>
      {
        this.imgPath=res[0]['user_profileimg'];
      },
      (err)=>
      {
        //console.log(err);
      }
      )
    this.crud.postData('/users/getuserskill',{user_id:getUserData['uid']}).subscribe(
      (res)=>
      {
        this.CandidateSkillsDashboard=res;
      },
      (err)=>
      { 
        //console.log(err);
      }

    )
    
    this.crud.postData('/users/getuserdata',{user_id:getUserData['uid']}).subscribe(
      (res)=>
      {
        this.rating_avg=res[0]['rating_avt'];
        this.profession=res[0]['rating_pro'];
        this.experience=res[0]['rating_exp'];
      
       
       
      },
      (err)=>
      { 
        //console.log(err);
      }

    )
  }
  }
    

}
