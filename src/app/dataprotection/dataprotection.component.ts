import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {AuthService} from '../auth.service';
import { CrudService } from '../crud.service';
import { DataservicesService } from '../dataservices.service';
@Component({
  selector: 'app-dataprotection',
  templateUrl: './dataprotection.component.html',
  styleUrls: ['./dataprotection.component.css']
})
export class DataprotectionComponent implements OnInit {
  showmenu: boolean;

  constructor(private translate: TranslateService,private auth:AuthService,public crud:CrudService,public dt:DataservicesService) { 
    translate.setDefaultLang('en');

  }
  switchLanguage(language: string) {
    this.translate.use(language);
  }

  ngOnInit() {
    this.showmenu=false
    this.auth.getLoggedInDetails();
    if(this.auth.getLoggedInDetails())
    {
      this.showmenu=true;
    }
  }
  logout()
  {
    this.crud.select('/users/logout').subscribe((res)=>{
      if(res=='done')
      {
        this.auth.logout()
        this.dt.loginDetails(false);
        // this.router.navigate(['']);
        window.location.href='/';
      }

     },(err)=>{

      })
  }

}
