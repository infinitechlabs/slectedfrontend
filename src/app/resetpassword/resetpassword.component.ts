import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../navbar.service';
import { DataservicesService } from '../dataservices.service';
import {NgForm, FormGroup,FormBuilder,Validators} from '@angular/forms';
import { CrudService } from '../crud.service';
import { AuthService } from '../auth.service';
import { Router,ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
  saftyForm
  msg: any;
  status:any;
  constructor(public nav: NavbarService,public dt:DataservicesService,private fb:FormBuilder,private crud:CrudService,private auth:AuthService,private router:Router, private Activaterouter:ActivatedRoute) {
    this.nav.BeforeLogin(true);
    this.saftyForm=fb.group({
      oldPass:['',Validators.required],
      newPass:['',Validators.required],
      confirmPass:['',Validators.required]
    })

   }

  ngOnInit() {
  }
  SubmitData(value)
  {
    var id= this.Activaterouter.snapshot.params;
    
    let getUserData=this.auth.getLoggedInDetails();
    if(value.newPass == value.confirmPass){
      var formValue={
        'user_id':getUserData['uid'],
        'user_pass':value.newPass
        // 'user_dob':value.confirmPass
        
       }
        this.crud.postData('/users/updatePassword',formValue).subscribe((res) =>{
          //console.log(res);
          this.msg="Password update";
        
        },
        (err) =>{
        
        
        }
      );
    }
    else{
      this.msg="Password mismatch";
    }
  }

}
