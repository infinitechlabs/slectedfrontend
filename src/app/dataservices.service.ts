import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataservicesService {
  public data_trans_object = new Subject();
  public data_trans_object2 = new Subject();
  constructor() { }
  shareData(rec)
  {
    this.data_trans_object.next(rec)
  }
  loginDetails(username)
  {
    this.data_trans_object2.next(username)
  }
}
