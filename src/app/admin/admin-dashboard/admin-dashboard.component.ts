import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { CrudService } from 'src/app/crud.service';
import { DataservicesService } from 'src/app/dataservices.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  basePath: string;
  websitePath: string;
  ngOfusers: any;
  listofUsers: Object;

  constructor(public dt:DataservicesService,public auth:AuthService,public crud:CrudService,private http: HttpClient,private router:Router) { 
    this.basePath=environment.serverPath;
    this.websitePath=environment.websitePath;


  }
  ngOnInit() {
    this.usersCount();
    this.usersList();
  }
  usersCount(){
    this.crud.postData("/users/registerd_user_count",{"id":1}).subscribe((res)=>{
      //console.log(res);
      this.ngOfusers=res[0].cnt;
      
    })
  }
  usersList(){
    this.crud.postData("/users/registerd_user_list",{"id":1}).subscribe((res)=>{
      //console.log(res);
      this.listofUsers=res;
    })
  }
  logout()
  {
    this.crud.select('/users/logout').subscribe((res)=>{
      if(res=='done')
      {
        this.auth.logout()
        this.dt.loginDetails(false);
        // this.router.navigate(['']);
        window.location.href='/admin/login';
      }

     },(err)=>{

      })
  }
}
