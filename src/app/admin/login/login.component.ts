import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { CrudService } from 'src/app/crud.service';
import { NavbarService } from 'src/app/navbar.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  passwordForm:FormGroup;  
  errmsg: boolean;
  constructor(public nav: NavbarService,private auth:AuthService,private fb:FormBuilder,private crud:CrudService,private router:Router) {
    this.nav.BeforeLogin(true)
    this.passwordForm=fb.group({
      email:['',[Validators.required]],
      password:['',[Validators.required]]
     
    })
  }
  ngOnInit() {
  }
  PostData(value)
    {
      //const password=value.password;
      //let getEmail=this.auth.getLoggedInDetails();
      var formValue={
              'email':this.passwordForm.value.email,
              'password':this.passwordForm.value.password
          }
        this.crud.postData('/users/adminlogin',formValue).subscribe(
          res =>{
            this.router.navigate(['/admin/dashboard'])
            
          },
          err =>{
            this.errmsg=true;
          setInterval(() => {  
           this.errmsg=false;
              
            },3000);
           
           
          }
        );
        
    }
}
