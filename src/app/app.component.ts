import { Component } from '@angular/core';
import { NavbarService } from './navbar.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
  show: any=true;
  hide: any=true
  ;
  imagepath: string;
  //show: any;
  
  constructor(public nav: NavbarService) { }
  ngOnInit() {
    this.imagepath="locationhost";
    this.nav.afterLoginScreen.subscribe(res =>{
      //console.log(res);
      this.show=res;
      this.hide=false;
    })
    this.nav.beforeLoginScreen.subscribe(res=>{
      //console.log(res);
      this.show=false
      this.hide=res;
    })
  }  

}
