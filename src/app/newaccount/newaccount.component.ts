import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../navbar.service';
import { RouterModule,Router } from '@angular/router';
import {FormBuilder,FormGroup,FormControl,Validators,NgForm} from '@angular/forms'
import {AuthService} from '../auth.service';
import { CrudService } from '../crud.service';
@Component({
  selector: 'app-newaccount',
  templateUrl: './newaccount.component.html',
  styleUrls: ['./newaccount.component.css']
})
export class NewaccountComponent implements OnInit  {
  signUpForm:FormGroup;
  msg: any;
  status:any;
  public account = {  
    
        password: null  
      };  
      public barLabel: string = "";

  constructor(public nav: NavbarService,private auth:AuthService,private fb:FormBuilder,private crud:CrudService,private router:Router) { 
    this.nav.BeforeLogin(true);
    this.signUpForm=fb.group({
      userName:['',[Validators.required,Validators.pattern('^([a-zA-Z]{2,}\\s[a-zA-z]{1,}?-?[a-zA-Z]{2,})')]],
      password:['',[Validators.required]],
      privacyPolicy:['',[Validators.required, ]],
    })  
  }

  ngOnInit() {
  }
  PostData(value)
  {
   
    let getEmail=this.auth.getLoggedInDetails();
      
      var formValue={
              'user_email':getEmail['email'],
              'fullname':value['userName'],
              'password':value['password'],
          }
      
      this.crud.postData('/users/register',formValue).subscribe(
        res =>{
          
         
          if (this.auth.storeToken(res) === true ){
            this.router.navigate(['/rating']);
          }
        },
        err =>{
         
          this.msg=err.error;
         
        }
      );    
  }
}

  