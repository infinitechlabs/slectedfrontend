import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../navbar.service';
import { RouterModule,Router } from '@angular/router';
import {FormBuilder,FormGroup,FormControl,Validators,NgForm} from '@angular/forms'
import {AuthService} from '../auth.service';
import { CrudService } from '../crud.service';

import {trigger, style, animate, transition} from '@angular/animations';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css'],
  
})
export class PasswordComponent implements OnInit {
  passwordForm:FormGroup;  
  msg: string;
  errmsg: boolean;

  constructor(public nav: NavbarService,private auth:AuthService,private fb:FormBuilder,private crud:CrudService,private router:Router) {
    this.nav.BeforeLogin(true)
    this.passwordForm=fb.group({
      
      password:['',[Validators.required]]
     
    })
  }
  userFullName
  
  ngOnInit() {
    let res=this.auth.getLoggedInDetails()
    this.userFullName=res['fullname'];
  }

  PostData(value)
    {
      const password=value.password;
      
      let getEmail=this.auth.getLoggedInDetails();
      
      var formValue={
              'email':getEmail['email'],
              'password':password
          }
      
        this.crud.postData('/users/login',formValue).subscribe(
          res =>{
            if (this.auth.storeToken(res) === true ){
              this.crud.postData('/users/useTotExp',{user_id:getEmail['uid']}).subscribe(
                (res)=>{
                  
                  if(res==0)
                  {
                    this.router.navigate(['/rating'])
                  }
                  else
                  {
                    this.router.navigate(['/dashboard'])
                  }
                  
                },
                (err)=>
                {

                }

              )
             
              
            }
          },
          err =>{
            
            this.errmsg=true;
            setInterval(() => {  
              this.errmsg=false;
              
            },3000);
           
           
          }
        );
        
    }

}
