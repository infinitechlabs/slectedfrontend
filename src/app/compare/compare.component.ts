import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Options } from 'ng5-slider';
import { environment } from '../../environments/environment';
import { NavbarService } from '../navbar.service';
import { DataservicesService } from '../dataservices.service';
import { AuthService } from '../auth.service';
import { CrudService } from '../crud.service';
import { NumberformatePipe } from '../numberformate.pipe';
declare var $: any;
declare var $:any
@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.css']
})
export class CompareComponent implements OnInit,AfterViewInit{
  skillDropdown: Object;
  expDropdown: Object;
  locationDropdown: any=[];
  keyword: string;
  CandidateList: Object;
  ExpLevel
  selectedLevel
  experience: number;
  skills: any;
  basePath:any;
  websitePath: string;
  location: string;
  constructor(public nav: NavbarService,public dt:DataservicesService,public auth:AuthService,public crud:CrudService,private formatPipe:NumberformatePipe) { this.nav.AferLogin(true)
    this.basePath=environment.serverPath;
    this.websitePath=environment.websitePath;
  }
  minValue: number=1
  maxValue: number=100
  value: number = 0;
  highValue: number = 100;
  options: Options = {
    floor: this.minValue,
    ceil: this.maxValue,
   
  };

  MinRange: number;
  MaxRange: number;
  selectedArea:string;


 
 
  ngOnInit() {
   
    // this.location="Location";
    /*Get Place list from masters*/
    this.crud.postData('/masters/getMaster',({master_type:4})).subscribe(
      (res:any)=>{
        this.keyword = 'name';
        let sel=[];
          for(let i=0;i<res.length;i++)
          {
            sel.push({id:res[i]['master_id'],name:res[i]['master_name']});
          }
          this.locationDropdown=sel;
          //this.selectedItems=sel;
       
        },
      (err)=>{
        //this.position="NA";
        //this.PreviousCompnay="NA";

      }
    ) 
    
    /*Get Candidate List*/
    this.crud.select("/users/compaireData").subscribe((res)=>{
      this.CandidateList=res;
    },
    (err)=>
      {
        
      }
    )
     /*Get Experice list from masters*/
     this.crud.postData('/masters/getMaster',({master_type:3})).subscribe(
      (res)=>{
        this.expDropdown=res
        },
      (err)=>{
        }
    ) 
    /*Get Master List*/
    this.crud.select('/masters/getSkill').subscribe(
      (res)=>{
        
        this.skillDropdown=res
        },
      (err)=>{
      }
    )
    /*Get Rating range*/
    this.crud.select('/masters/rating_range').subscribe(
      (res)=>{
       
        this.minValue=res[0]['min_rating'];
        this.maxValue =res[0]['max_rating'];
      },
      (err)=>{

      }
    )
    
  }
  selectEvent(item) {
    // do something with selected item
    //alert(item);
    this.selectedArea=item['name']
    
    this.getFilterData(this.selectedArea,this.MinRange,this.MaxRange,this.experience,this.skills)
  
  }
  clearAreaSearch(value)
  {
    
  } 
  sliderEvent()
  {
    this.MinRange=this.value;
    this.MaxRange=this.highValue
    this.getFilterData(this.selectedArea,this.MinRange,this.MaxRange,this.experience,this.skills)
  
  }
  Expselected(event:any)
  {
    this.experience=event.target.value;
    this.getFilterData(this.selectedArea,this.MinRange,this.MaxRange,this.experience,this.skills)
  
  }
  Skillselected(event:any)
  {
    this.skills=event.target.value;
    this.getFilterData(this.selectedArea,this.MinRange,this.MaxRange,this.experience,this.skills)
  
  }
  getFilterData(area: string,MinRange: number,MaxRange: number,exp: number,skills: any)
  {
    this.crud.postData1('/users/FiltercompareData',{rating_area:area,rating_Minrange:MinRange,rating_Maxrange:MaxRange,rating_exp:exp,rating_skills:skills}).subscribe((res)=>{
      this.CandidateList=res;
    },
    (err)=>
    {

    }
    )
  }
  onChangeSearch(val: string) {
    // this.skills=event.target.value;
    this.getFilterData(val,this.MinRange,this.MaxRange,this.experience,this.skills)
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }
  
  
  
  ngAfterViewInit(): void {

    
  }

  


}