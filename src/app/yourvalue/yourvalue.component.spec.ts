import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourvalueComponent } from './yourvalue.component';

describe('YourvalueComponent', () => {
  let component: YourvalueComponent;
  let fixture: ComponentFixture<YourvalueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourvalueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourvalueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
