import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../navbar.service';
import { TranslateService } from '@ngx-translate/core';
import {AuthService} from '../auth.service';
@Component({
  selector: 'app-yourvalue',
  templateUrl: './yourvalue.component.html',
  styleUrls: ['./yourvalue.component.css']
})
export class YourvalueComponent implements OnInit {
  showmenu: boolean;

  constructor(public nav: NavbarService,private translate: TranslateService,private auth:AuthService) {
    this.nav.BeforeLogin(true);
    translate.setDefaultLang('en');
   }
   switchLanguage(language: string) {
    this.translate.use(language);
  }
  ngOnInit() {
    this.showmenu=false
    this.auth.getLoggedInDetails();
    if(this.auth.getLoggedInDetails())
    {
      this.showmenu=true;
    }
    
  

    
  }

}
