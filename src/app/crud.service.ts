import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private httpData:HttpClient) { }
//private url='http://api.slected.me';
//private url='http://localhost:3000';
private url='http://ec2-18-217-236-136.us-east-2.compute.amazonaws.com:3000';
  select(urlname: string)
  {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    return this.httpData.get(this.url+urlname);
  }
  postData(routename,data)
  {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded'
        // 'Content-Type':  'application/application/json'
        })
    };
    return this.httpData.post(this.url+routename,data);
  }
  postImage(routename,data)
  {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded'
        //'Content-Type':  'application/application/json'
        })
    };
    return this.httpData.post(this.url+routename,data);
  }
  postData1(routename,data)
  {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
        'Content-Type':  'application/application/json'

      })
    };
    return this.httpData.post(this.url+routename,data);
  }
  socialLink(data)
  {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
        'Content-Type':  'application/application/json'

      })
    };
    
     const socialLogin="https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&code="+data+"&redirect_uri=http://localhost:4200/linkedInLogin&client_id=819o2a4wpocvlt&client_secret=Eiia9et6ehIcPxn7"
    return this.httpData.post(socialLogin,data);
  }
  
  socialUrl(routename,accessToken)
  {
    //console.log(accessToken);
    const httpOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
        'Authorization':  'Bearer'+accessToken

      })
    };
    return this.httpData.post(routename,"");
  }
}
