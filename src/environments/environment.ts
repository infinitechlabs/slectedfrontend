// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: false,
  /*Local */
  //serverPath:'http://localhost:3000',
  //websitePath:'http://localhost:4200/',
  
  /*Testing Server*/  
  ///serverPath:'http://ec2-18-217-236-136.us-east-2.compute.amazonaws.com:3000',
  //websitePath:'http://infinitechlabs.in/v6/',

  /*Live Server*/
  serverPath:'https://ec2-18-217-236-136.us-east-2.compute.amazonaws.com:3000',
  //serverPath:'http://api.slected.me',
   //websitePath:'http://slected.me/v2'

  websitePath:'http://slected.me/' 
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.